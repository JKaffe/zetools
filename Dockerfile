# Copyright (C) 2022 Karmjit Mahil.
#
# This file is part of Zetools.
#
# Zetools is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Zetools is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Zetools. If not, see <https://www.gnu.org/licenses/>.
#

FROM ubuntu:22.04
MAINTAINER JKaffe

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN apt update && \
	DEBIAN_FRONTEND="noninteractive" \
	TZ="Europe/London" \
	apt -y install libncurses-dev libgnutls28-dev sed make binutils build-essential diffutils gcc g++ bash patch gzip bzip2 perl tar cpio unzip rsync file bc findutils wget git python-is-python3

COPY build-zed /bin
COPY buildroot-config /etc/buildroot-config
COPY uboot.script /etc/uboot-script

RUN useradd -m -d /home/nonroot -u 1000 nonroot
USER nonroot

CMD ["/bin/build-zed"]
