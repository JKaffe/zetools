# Zetools.

## Zebuilder.
Buildroot based linux build in docker targeting the Zedboard FPGA board.

### Get up and running.
Create the docker image.
```
$ sudo docker build -t zebuilder .
```
Kick off the build. This might take a while depending on the machine you're building on as well as your internet connection.
```
$ mkdir -p build
$ sudo docker run --name zebuilder-container --mount "type=bind,source=${PWD}/build,target=/home/nonroot" zebuilder
```
Create the FIT image.
```
$ ./build-itb
```
Flash an sd card.
```
$ sudo ./setup-mmc
```
And you're good to go.


The default root password is `zed`.


By default you should be able to access the Zedboard through the board's usb serial connection or through ssh (once Linux is up and running).

### Toolchain.
TODO.

### Known issues.
 - No root ssh access by default. The user has to `echo "PermitRootLogin yes" >> /etc/ssh/sshd_config` on the first boot to gain ssh access. TODO: Either setup a non root user, copy host ssh keys to use keys instead, or set the option on by default.

 - `spl_load_image_fat_os`. At boot the U-Boot SPL is looking for specific files to boot Linux. These files cannot be found as the setup uses a FIT image instead. Eventually the SPL should be patched or somehow reconfigured so that it uses the FIT image directly.

 - U-Boot autoboot fails. For now, once the user is dropped into the U-Boot command line, they have to manually copy in and run lines from `uboot.script`. Eventually this should be fixed so that Linux gets booted automatically.

## Licensing.
The project is licensed under GPL3+.
