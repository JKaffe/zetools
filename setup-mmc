#!/bin/bash

# Copyright (C) 2022 Karmjit Mahil.
#
# This file is part of Zetools.
#
# Zetools is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Zetools is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Zetools. If not, see <https://www.gnu.org/licenses/>.
#

IMAGE_DIR="build/buildroot/output/images"

[ "$(whoami)" != 'root' ] && printf 'Error: This script requires super user permission to format the sd card.\n' && exit 1

OLD_PATH="$PATH"

# Use only tools built by buildroot so we don't rely on possibly uncommon tools in the host environment.
PATH="$PWD/build/buildroot/output/host/usr/sbin:$PWD/build/buildroot/output/host/usr/bin"

lsblk
printf '\n'
read -r -p 'Enter SD card: ' MMC

[ -b "$MMC" ]
[ "$?" -ne "0" ] && printf "Invalid device: \"$MMC\".\n" && exit 1

printf "\n"
printf "All data on $MMC will be lost.\n"
read -r -p 'Do you want to continue (y/n): ' APPROVAL
printf '\n'

[ "$APPROVAL" != 'y' ] && printf 'Exiting.\n' && exit 0

sfdisk $MMC << EOF
1, 100M, 0xC, *
100M, , ,
write
EOF

/usr/bin/sync "${MMC}"

mkfs.vfat -F 32 "${MMC}1"
mount "${MMC}1" /mnt

/usr/bin/cp "$IMAGE_DIR/boot.bin" /mnt
/usr/bin/cp "$IMAGE_DIR/u-boot.img" /mnt
/usr/bin/cp "$IMAGE_DIR/fit.itb" /mnt
/usr/bin/cp "$IMAGE_DIR/boot.scr" /mnt

umount /mnt

/usr/bin/dd if="$IMAGE_DIR"/rootfs.ext2 of="${MMC}2" bs=4M status=progress

printf '\nExpanding rootfs to the whole partition.\nThis may take a while.\n\n'

e2fsck -f "${MMC}2"
resize2fs -p "${MMC}2"

PATH="$OLD_PATH"
